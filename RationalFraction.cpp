#include "RationalFractions.h"
#include <iostream>

RationalFraction::RationalFraction() : RationalFraction(1, 1)
{
}

RationalFraction::RationalFraction(int numerator, int denominator) {
	this->setDenominator(denominator);
	this->setNumerator(numerator);

	this->reduction();
}

int RationalFraction::getNumerator() const
{
	return this->numerator;
}

int RationalFraction::getGCD()
{
	int a = abs(this->getNumerator()), b = abs(this->getDenominator());

	while (a > 0 && b > 0) {

		if (a > b) {
			a %= b;
		}
		else {
			b %= a;
		}
	}

	return a + b;
}

void RationalFraction::reduction()
{
	int gcd = this->getGCD();

	this->setDenominator(this->getDenominator() / gcd);
	this->setNumerator(this->getNumerator() / gcd);
}

int RationalFraction::getDenominator() const
{
	return this->denominator;
}

void RationalFraction::setNumerator(int a)
{
	this->numerator = a;
}

void RationalFraction::setDenominator(int a)
{
	if (a == 0)
	{
		throw std::out_of_range("denominator is invalid");
	}

	this->denominator = a;
}

void RationalFraction::exponentiation(int a)
{
	this->setDenominator(pow(this->getDenominator(), a));
	this->setNumerator(pow(this->getNumerator(), a));
}

void RationalFraction::multiplicationByNum(int a)
{
	this->setNumerator(this->getNumerator() * a);

	this->reduction();
}

void RationalFraction::displayFraction()
{
	std::cout << "This fraction is: " << this->getNumerator() << " / " << this->getDenominator() << std::endl;
}

RationalFraction RationalFraction::sum(const RationalFraction& first, const RationalFraction& second)
{
	int resNumerator = first.getNumerator() * second.getDenominator() + second.getNumerator() * first.getDenominator();
	int resDenominator = first.getDenominator() * second.getDenominator();

	RationalFraction result(resNumerator, resDenominator);

	return result;
}

RationalFraction RationalFraction::multiplication(const RationalFraction& first, const RationalFraction& second)
{
	int resNumerator = first.getNumerator() * second.getNumerator();
	int resDenominator = first.getDenominator() * second.getDenominator();

	RationalFraction result(resNumerator, resDenominator);

	return result;
}

RationalFraction operator+(const RationalFraction& first, const RationalFraction& second)
{
	return RationalFraction::sum(first, second);
}

RationalFraction operator*(const RationalFraction& first, const RationalFraction& second)
{
	return RationalFraction::multiplication(first, second);
}
