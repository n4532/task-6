#pragma once

class RationalFraction
{
public:
	RationalFraction();
	RationalFraction(int, int);
	void setNumerator(int);
	void setDenominator(int);
	void exponentiation(int);
	void multiplicationByNum(int);
	void displayFraction();
	friend RationalFraction operator + (const RationalFraction&, const RationalFraction&);
	friend RationalFraction operator * (const RationalFraction&, const RationalFraction&);
	static RationalFraction sum(const RationalFraction&, const RationalFraction&);
	static RationalFraction multiplication(const RationalFraction& first, const RationalFraction& second);
private:
	int numerator;
	int denominator;
	int getDenominator() const;
	int getNumerator() const;
	int getGCD();
	void reduction();
};

