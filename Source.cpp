#include <iostream>
#include "RationalFractions.h"

using namespace std;

int main() {
	RationalFraction first(12, 5);
	RationalFraction second(4, 8);

	first.multiplicationByNum(3);
	first.displayFraction();

	second.exponentiation(2);
	second.displayFraction();

	RationalFraction third = first + second;
	third.displayFraction();

	third = first * second;
	third.displayFraction();

	return 0;
}